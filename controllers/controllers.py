# -*- coding: utf-8 -*-

from odoo import http

import logging
_logger     = logging.getLogger( __name__ )


class GscController(http.Controller):
     @http.route('/accept_gsc/default/', auth='public', website=True)
     def index(self, **kw):
         _logger.info("Accept GSC index showing!")
         default_text= "Hello, conditions générales de vente ... !"
         http.request.env.cr.execute("SELECT text FROM accept_gsc WHERE id=1;" )
         default_text = http.request.env.cr.fetchall()[0][0]
         #_logger.info( default_text )
         return default_text
         #return "Hello, conditions générales de vente!"

#     @http.route('/accept_gsc/accept_gsc/default/', auth='none', website=True)
#     def default(self, **kw):
#         _logger.info("Accept GSC showing the default!")
#         self.env.cr.execute("SELECT text FROM accept_gsc WHERE id=1;" )
#         default_text = self.env.cr.fetchall()[0][0]
#         _logger.info( default_text )
#         return default_text

#     @http.route('/accept_gsc/accept_gsc/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('accept_gsc.listing', {
#             'root': '/accept_gsc/accept_gsc',
#             'objects': http.request.env['accept_gsc.accept_gsc'].search([]),
#         })

#     @http.route('/accept_gsc/accept_gsc/objects/<model("accept_gsc.accept_gsc"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('accept_gsc.object', {
#             'object': obj
#         })