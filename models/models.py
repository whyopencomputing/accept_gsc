# -*- coding: utf-8 -*-

from odoo import models, fields, api

import logging
_logger     = logging.getLogger( __name__ )


class AcceptGsc(models.Model):
     _name  = 'accept.gsc'
     text   = fields.Html( string="General Sales Condition"     ) 
     
     #@api.one
     #def get_default(self): 
     #   _logger.info("Selecting the standard GSC!")
     #   cr.execute("SELECT text FROM accept_gsc WHERE id=1;" )
     #   default_text = cr.fetchall()[0][0]
     #   _logger.info( default_text )
     #   return default_text

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100