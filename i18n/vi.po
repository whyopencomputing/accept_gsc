# Translation of Odoo Server.
# This file contains the translation of the following modules:
# * payment_postfinance
# 
# Translators:
# fanha99 <fanha99@hotmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 10.0c\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-09-07 08:57+0000\n"
"PO-Revision-Date: 2016-09-07 08:57+0000\n"
"Last-Translator: fanha99 <fanha99@hotmail.com>, 2016\n"
"Language-Team: Vietnamese (https://www.transifex.com/odoo/teams/41243/vi/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Language: vi\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. module: payment_postfinance
#: model:ir.model.fields,field_description:payment_postfinance.field_payment_acquirer_postfinance_api_access_token
msgid "Access Token"
msgstr ""

#. module: payment_postfinance
#: model:ir.model.fields,field_description:payment_postfinance.field_payment_acquirer_postfinance_api_access_token_validity
msgid "Access Token Validity"
msgstr ""

#. module: payment_postfinance
#: model:ir.ui.view,arch_db:payment_postfinance.acquirer_form_postfinance
msgid "How to configure your postfinance account?"
msgstr ""

#. module: payment_postfinance
#: model:ir.model,name:payment_postfinance.model_payment_acquirer
msgid "Payment Acquirer"
msgstr "Payment Acquirer"

#. module: payment_postfinance
#: model:ir.model,name:payment_postfinance.model_payment_transaction
msgid "Payment Transaction"
msgstr ""

#. module: payment_postfinance
#: model:ir.model.fields,field_description:payment_postfinance.field_payment_acquirer_postfinance_email_account
msgid "Postfinance Email ID"
msgstr ""

#. module: payment_postfinance
#: model:ir.model.fields,help:payment_postfinance.field_payment_acquirer_postfinance_use_ipn
msgid "Postfinance Instant Payment Notification"
msgstr ""

#. module: payment_postfinance
#: model:ir.model.fields,field_description:payment_postfinance.field_payment_acquirer_postfinance_seller_account
msgid "Postfinance Merchant ID"
msgstr ""

#. module: payment_postfinance
#: model:ir.ui.view,arch_db:payment_postfinance.transaction_form_postfinance
msgid "Postfinance TX Details"
msgstr ""

#. module: payment_postfinance
#: code:addons/payment_postfinance/models/payment.py:134
#, python-format
msgid "Postfinance: received data with missing reference (%s) or txn_id (%s)"
msgstr ""

#. module: payment_postfinance
#: model:ir.model.fields,field_description:payment_postfinance.field_payment_acquirer_postfinance_api_password
msgid "Rest API Password"
msgstr ""

#. module: payment_postfinance
#: model:ir.model.fields,field_description:payment_postfinance.field_payment_acquirer_postfinance_api_username
msgid "Rest API Username"
msgstr ""

#. module: payment_postfinance
#: model:ir.model.fields,help:payment_postfinance.field_payment_acquirer_postfinance_seller_account
msgid ""
"The Merchant ID is used to ensure communications coming from Postfinance are "
"valid and secured."
msgstr ""

#. module: payment_postfinance
#: model:ir.model.fields,field_description:payment_postfinance.field_payment_transaction_postfinance_txn_type
msgid "Transaction type"
msgstr ""

#. module: payment_postfinance
#: model:ir.model.fields,field_description:payment_postfinance.field_payment_acquirer_postfinance_use_ipn
msgid "Use IPN"
msgstr ""

#. module: payment_postfinance
#: model:ir.model.fields,field_description:payment_postfinance.field_payment_acquirer_postfinance_api_enabled
msgid "Use Rest API"
msgstr ""
