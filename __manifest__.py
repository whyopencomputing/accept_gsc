# -*- coding: utf-8 -*-
{

    'name': "accept_gsc",
    
    'summary': """
        Accept General Sales Conditions
        """,
    
    'description': """
        Put a checkbox on the payment page to accept the GSC
    """,

    'author':'Why! Open Computing, Jean Tinguely Awais',
    'website':'http://www.whyopencomputing.ch',
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Web',
    'version': '1.0',
    # any module necessary for this one to work correctly
    'depends': ['website','website_sale','website_sale_delivery'],
    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/accept_gsc_config.xml',
        'views/accept_gsc_web.xml',
        'data/gsc.xml',
    ],
    'installable': True,

}